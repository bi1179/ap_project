typedef struct {
    unsigned int sel_id;       /* ex, 1 ~ 99 */
    struct sockaddr_in clnt_addr;
    int socket_fd;
    char nick[30];
}socket_map;

socket_map smap[10];
int smap_cnt=0;

void smap_init(){
    
}

int find_empty_sock() {
    int a;
    for (a = 0; a < 9; a++)
        if (smap[a].sel_id == 0)	return a;
    return -1;  // not found
}

int isEmpty(int index) {
    return (smap[index-1].sel_id == 0);
}

void smap_add(struct sockaddr_in clnt,int fd, char *nik) {
    
    int nul = find_empty_sock();
    int max, min, a;
    
    max = smap[0].sel_id;
    for (a = 1; a < 9; a++)
        if (smap[a].sel_id > max) max = smap[a].sel_id;
    max++;
    
    smap[nul].sel_id = max;
    smap[nul].clnt_addr = clnt;
    smap[nul].socket_fd = fd;
    strncpy(smap[nul].nick, nik, strlen(nik)+1);
    smap_cnt = max;
}

void remove_sock(int id) {
    int a;
    for (a = 0; a < 9; a++)
        if (smap[a].sel_id == id) {
            smap[a].sel_id = 0;
            close(smap[a].socket_fd);
        }
}

void show_smaps(){
    int i;
    
    if(smap_cnt == 0){
        printf("| #%d |  %s  |  %s  |\n", smap[0].sel_id, smap[0].nick, inet_ntoa(smap[0].clnt_addr.sin_addr));
    }else{
        for(i=0;i<smap_cnt;i++){
            printf("| #%d |  %s  |  %s  |\n", smap[i].sel_id, smap[i].nick, inet_ntoa(smap[i].clnt_addr.sin_addr));
        }
    }
    printf("\n\n");
}