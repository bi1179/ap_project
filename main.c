#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#include <pthread.h>
#include <regex.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <fcntl.h>

/* for get ip address from eth0*/
#include <net/if.h>
#include <sys/ioctl.h>

pthread_mutex_t mux;

#include "trim.h"
#include "Functions.h"
#include "MsgStruct.h"
#include "smap.h"
#include "ftp.h"


#define NAME_BUF_SIZE 30


void doSend_ip(char target[], char type, char nick[], char msg[]){
    char *myip;
    msg_t buf;

    myip = getEth_ip();
    if(type == 'c'){
        strcpy(buf.src_ip,myip );  //임시
        buf.type = 'm';
        strncpy(buf.who, nick, 30);
        
        Mth_Send_IMW(target, buf);
        
        printf("[+] ask to (%s) for chatting...\n", target);
    }
    /*else if(type == 'm'){
        
        // 존재한다면( #넘버로)
        
        
    }else if(type == 'f'){
     
         //File transfer
     
        
    }*/else{
        printf("\ntype: c - connect, m - message, f - file transfer\n%s> [dst addr] [type] [message/file]\n------------------------------------------\n\n", nick);
    }
}

void doSend_sel(char target[], char type, char nick[], char msg[]){
    char *myip;
    msg_b buf;
    int sel_id = atoi(target);
    pthread_t file_sender;
    
    ftp_info info;
    strncpy(info.ip, inet_ntoa(smap[sel_id].clnt_addr.sin_addr), 16);
    info.fname = msg;
    /*strncpy(myip, inet_ntoa(smap[sel_id].clnt_addr.sin_addr), 16);
    strncpy(info.fname, msg, 256);
    strncpy(info.ip, myip, 16);
    */
    if(type == 'm' && !(isEmpty(sel_id))){
        // 존재한다면(ip로 혹은 #넘버로)
        strncpy(buf.who, nick, 30);
        strncpy(buf.msg, msg, 2048);
        
        send_msg(smap[sel_id-1].socket_fd, buf);
        printf("message: %s\n", buf.msg);
    }else if(type == 'f' && !(isEmpty(sel_id))){
        /*
         File transfer
         */
        pthread_create(&file_sender, NULL, &ftp_clnt_thd, (void*)&info);
        
        
    }else{
        printf("\ntype: c - connect, m - message, f - file transfer\n%s> [dst addr] [type] [message/file]\n------------------------------------------\n\n", nick);
    }
}


/*
	Command input wait thread.
	this is main thread.
    will functionaly send_msg()
*/
void mainthrd(char *arg){
    char nick[30];
    int num,i;
    char buff[16];

    int q_i=0;
    char str[1000];
    char **tmp=NULL;
    char tm[128];

    
    strcpy(nick, arg);	// for escape 'segmentation fault'
    
    printf("type: c - connect, m - message, f - file transfer\n%s> [dst addr] [type] [message/file]\n\n",nick);
    
	while (true){
        memset(str, 0x00, 0);
        memset(tmp, 0x00, 0);
        
        pthread_mutex_lock(&mux);
		printf("%s> ", nick);
        pthread_mutex_unlock(&mux);
        fgets(str, sizeof(str), stdin);	// for unformated input

        
		// parsing
        tmp = str_split(trim(str), ' ', &num);	// trim for delete '\0'

		// do
		if (num == 1 && strlen(tmp[0]) == 1){ printf("Insert Command or type \'help\'\n");}

		else if ((strcmp(tmp[0], "exit")) == 0){
			printf("\ngood bye~\n");
			break;
		}        
		else if ((strcmp(tmp[0], "help")) == 0){
			printf("\ntype: c - connect, m - message, f - file transfer\n%s> [dst addr] [type] [message/file]\n------------------------------------------\n", nick);
			printf("list : show every connected user\n");
            printf("myip : check my ip address\n");
			printf("help : show manual\n");
			printf("exit : close this program\n\n");
		}
		else if ((strcmp(tmp[0], "list")) == 0){
			printf("Connected user list\n");
            show_smaps();
		}
        else if ((strcmp(tmp[0], "myip")) == 0){
            printf("My Ip address\n");
            printf("My IP: %s\n", getEth_ip());
        }else{

            if(num > 3){
                for(i=3; i<num;i++){
                    strncat(tm, " ",1);
                    strncat(tm, tmp[i], sizeof(tmp[i]));
                }
                strncat(tmp[2], tm, 1024);
            }
            
            if (match(tmp[0]) == true && num >= 2){
                // Server start or send message.
                
                // separated message merge.
                memset(buff, 0x00, 0);

                strncpy(buff, tmp[0], 16);      // 주소 글자가 복사 될때 깨짐을 방지
                if(num < 2)
                    doSend_ip(buff, tmp[1][0], nick, NULL);
                else{
                    doSend_ip(buff, tmp[1][0], nick, tmp[2]);
                }
            }else if(match2(tmp[0]) == true && num >= 2){
                // # 으로 시작하는 메세지 전달 일경우.
                memset(buff, 0x00, 0);
                buff[0]=tmp[0][1];
                buff[1]=tmp[0][2];
                
                doSend_sel(buff,tmp[1][0], nick, tmp[2]);
            }
            else{
                printf("Unknown command\n");
            }
		}
	}
    exit(0);
}


int main(int argc, char ** argv){
	int i;
	int status;

    pthread_t imw_thrd, chat_serv, ftp_serv;
    
	//pthread_t thrds[6];

	char username[NAME_BUF_SIZE] = "";
	
	if (argc == 2){
		if (strlen(argv[1]) > 30){
			do_error("Nick name should be less than 30 lengths.\n");
		}
		strcpy(username, argv[1]);
	}
	else{
		do_error("Use > ./p2c [Nick name]\n");
	}
    
    // smap init
    smap_init();
    
    
    // thread prepare
    pthread_create(&imw_thrd, NULL, &IMW, (void*)username);
    pthread_create(&chat_serv, NULL, &server_thrd, NULL);
    pthread_create(&ftp_serv, NULL, &ftp_server_thd, (void*)username);
    
    // mutex prepare
    pthread_mutex_init(&mux, NULL);

    mainthrd(username);

	return 0;
}

void* IMW(void *arg){
    char *nick = (char*)arg;
    
    char buffer[1024];
    msg_t buf;
    int sockfd;
    int client;
    int state;
    int n;
    struct sockaddr_in serveraddr, clientaddr;
    
    char sel;
    
    
    client = sizeof(clientaddr);
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if(sockfd < 0){
        perror("socket error");
        pthread_exit(0);
    }
    
    bzero(&serveraddr, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
    serveraddr.sin_port = htons(60070);
    
    state = bind(sockfd, (struct sockaddr*)&serveraddr, sizeof(serveraddr));
    if(state==-1){
        perror("bind error : ");
        pthread_exit(0);
    }
    //printf("IMW Running,,,\n");
    
    while(1){
        n = recvfrom(sockfd, (void*)&buf, sizeof(buf), 0, (struct sockaddr*)&clientaddr, &client);
        
        if(buf.type == 'e'){
            pthread_mutex_lock(&mux);
            printf("[-] %s(%s) 님이 대화를 거절 했습니다.\n", buf.who, buf.src_ip);
            pthread_mutex_unlock(&mux);
        }else if(buf.type == 'm'){
            
            pthread_mutex_lock(&mux);
            printf("[!!]%s(%s)님이 채팅을 신청합니다.\n", buf.who, buf.src_ip);
            printf("승락 하시겠습니까?(Y/n)");
            scanf("%c", &sel);
            pthread_mutex_unlock(&mux);
            
            if(sel == 'y' || sel == 'Y'){
                // 채팅 서버에 접속
                client_thrd(buf.src_ip);
                pthread_mutex_lock(&mux);
                printf("[+] %s(%s)와 채팅시작!\n", buf.who, buf.src_ip);
                pthread_mutex_unlock(&mux);
            }else{
                // 거부 메세지 보내기.
                buf.type = 'e';
                strncpy(buf.who, nick, 30);
                
                Mth_Send_IMW(buf.src_ip, buf);
            }
        }
        
    }
    close(sockfd);
    
    
    pthread_exit((void*)0);
}

void *server_thrd(void *args){
    //association_t *inform = (struct association_t*)args;
    
    int serv_sock, clnt_sock;   // 단일 서버 소켓
    int clnt_adr_sz;
    pthread_t snd_thread, rcv_thread;
    void *thread_return;
    struct sockaddr_in serv_adr, clnt_adr;
    
    serv_sock=socket(PF_INET, SOCK_STREAM, 0);
    
    memset(&serv_adr, 0, sizeof(serv_adr));
    serv_adr.sin_family=AF_INET;
    serv_adr.sin_addr.s_addr=htonl(INADDR_ANY);
    serv_adr.sin_port=htons(60071);
    
    if(bind(serv_sock, (struct sockaddr*) &serv_adr, sizeof(serv_adr))==-1)
        error_handling("bind() error");
    if(listen(serv_sock, 5)==-1)
        error_handling("listen() error");
    
    clnt_adr_sz=sizeof(clnt_adr);
    clnt_sock=accept(serv_sock, (struct sockaddr*)&clnt_adr,&clnt_adr_sz);
    
    smap_add(clnt_adr, clnt_sock, "test nick");
    
    /*
    pthread_create(&snd_thread, NULL, send_msg, (void*)&clnt_sock);
    pthread_join(snd_thread, &thread_return);
    */
    pthread_create(&rcv_thread, NULL, recv_msg, (void*)&clnt_sock);
    pthread_join(rcv_thread, &thread_return);
    close(serv_sock);
    
    pthread_exit((void*)0);
}

void client_thrd(char ip[]){
    int sock;
    struct sockaddr_in serv_addr;
    pthread_t rcv_thread;
    void * thread_return;
    
    sock = socket(PF_INET, SOCK_STREAM, 0);
    
    memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family=AF_INET;
    serv_addr.sin_addr.s_addr=inet_addr(ip);
    serv_addr.sin_port=htons(60071);
    
    if(connect(sock, (struct sockaddr*)&serv_addr, sizeof(serv_addr))== -1)
        error_handling("Connect error");
    
    smap_add(serv_addr, sock, "test nick");
    
    pthread_create(&rcv_thread, NULL, recv_msg, (void*)&sock);
    //pthread_join(rcv_thread, &thread_return);
    
    //close(sock);
    //pthread_exit((void*)0);
}
