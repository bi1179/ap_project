typedef struct{
    char ip[16];
    char *fname;
}ftp_info;

void *ftp_clnt_thd(void * arg){
    ftp_info* info = (ftp_info*)arg;
    int sock;
    struct sockaddr_in serv_addr;
    
    int fp;
    char buf[1024];
    int len;
    char fname[256];
    
    printf("\n[+]%s을 전송 합니다.\n", info->fname);
    strncpy(fname, info->fname, 256);
    
    sock=socket(PF_INET, SOCK_STREAM, 0);
    
    memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family=AF_INET;
    serv_addr.sin_addr.s_addr=inet_addr(info->ip);
    serv_addr.sin_port=htons(60078);
    
    if(connect(sock, (struct sockaddr*)&serv_addr, sizeof(serv_addr))==-1)
        error_handling("connect() error");
    
    fp = open(fname, O_RDONLY);
    if( fp == NULL)
        error_handling("File open Error()");
    
    send(sock, fname, 256, 0);
    
    while((len = read(fp, buf, 1024)) != 0 ){
        write(sock, buf, len);
    }
    
    if(shutdown(sock, SHUT_WR) == -1)
        error_handling("shutdown error");
    
    len = read(sock, buf, 1024);
    write(1, buf, len);
    
    close(fp);
    close(sock);
    pthread_exit((void*)0);
}

void *recv_file(void *arg){
    
    int fp;
    int len;
    int clnt_sock = *((int*)arg);
    char fname[256];
    char buf[1024];
    recv(clnt_sock, fname, 256, MSG_WAITALL);
    fp = open(fname, O_CREAT|O_WRONLY|O_TRUNC);
    if(fp == -1)
        error_handling("open error");
    
    while((len = read(clnt_sock, buf, 1024)) != 0){
        write(fp, buf, len);
    }
    write(clnt_sock, "Receive Complete\n", 20);
    close(fp);
    close(clnt_sock);
}

void *ftp_server_thd(void *arg){
    char *nick = (char*)arg;
    int serv_sock, clnt_sock;
    struct sockaddr_in serv_adr, clnt_adr;
    int clnt_adr_sz;
    pthread_t t_id[100];
    void *thread_return;
    int count=0;
    
    
    serv_sock=socket(PF_INET, SOCK_STREAM, 0);
    
    memset(&serv_adr, 0, sizeof(serv_adr));
    serv_adr.sin_family=AF_INET;
    serv_adr.sin_addr.s_addr=htonl(INADDR_ANY);
    serv_adr.sin_port=htons(60078);
    
    if(bind(serv_sock, (struct sockaddr*)&serv_adr, sizeof(serv_adr))==-1)
        error_handling("bind() error");
    if(listen(serv_sock, 5)==-1)
        error_handling("listen() error");
    
    clnt_adr_sz = sizeof(clnt_adr);
    
    while(1){
        clnt_sock=accept(serv_sock, (struct sockaddr*)&clnt_adr,&clnt_adr_sz);
        printf("[+] 파일을 전송 받습니다.\n");
        if(clnt_sock == -1)
            error_handling("accept error");
        
        pthread_create(&t_id[count], NULL, recv_file, (void*)&clnt_sock);
        count = (count++)%100;
    }
    return 0;
}

