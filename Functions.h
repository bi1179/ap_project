#define true 1
#define false 0

void do_error(char *msg){
	fprintf(stderr, msg);
	exit(1);
}

/*
	split by delimiter
 */
char** str_split( char* str, char delim, int* numSplits )
{
    char** ret;
    int retLen;
    char* c;
    
    if ( ( str == NULL ) ||
        ( delim == '\0' ) )
    {
        /* Either of those will cause problems */
        ret = NULL;
        retLen = -1;
    }
    else
    {
        retLen = 0;
        c = str;
        
        /* Pre-calculate number of elements */
        do
        {
            if ( *c == delim )
            {
                retLen++;
            }
            
            c++;
        } while ( *c != '\0' );
        
        ret = malloc( ( retLen + 1 ) * sizeof( *ret ) );
        ret[retLen] = NULL;
        
        c = str;
        retLen = 1;
        ret[0] = str;
        
        do
        {
            if ( *c == delim )
            {
                ret[retLen++] = &c[1];
                *c = '\0';
            }
            
            c++;
        } while ( *c != '\0' );
    }
    
    if ( numSplits != NULL )
    {
        *numSplits = retLen;
    }
    
    return ret;
}

/* check ip type*/
int match(char *sz){
    regex_t regex;
    int rv;
    regmatch_t matches[1];
    
    rv = regcomp(&regex, "^([0-9]|[1-9][0-9]|1([0-9][0-9])|2([0-4][0-9]|5[0-5]))."
                 "([0-9]|[1-9][0-9]|1([0-9][0-9])|2([0-4][0-9]|5[0-5]))."
                 "([0-9]|[1-9][0-9]|1([0-9][0-9])|2([0-4][0-9]|5[0-5]))."
                 "([0-9]|[1-9][0-9]|1([0-9][0-9])|2([0-4][0-9]|5[0-5]))$", REG_EXTENDED);
    if (rv != 0){
        do_error("regcomp failed with \n");
    }
    
    
    if (regexec(&regex, sz, 1, matches, 0) == 0){
        return true;
    }
    else{
        return false;
    }
}

/* check ip type*/
int match2(char *sz){
    regex_t regex;
    int rv;
    regmatch_t matches[1];
    
    rv = regcomp(&regex, "^#([0-9]{1,3})$", REG_EXTENDED);
    if (rv != 0){
        do_error("regcomp failed with \n");
    }
    
    
    if (regexec(&regex, sz, 1, matches, 0) == 0){
        return true;
    }
    else{
        return false;
    }
}

char* getEth_ip(){
    int fd;
    struct ifreq ifr;
    
    fd = socket(AF_INET, SOCK_DGRAM, 0);
    
    ifr.ifr_addr.sa_family = AF_INET;
    
    strncpy(ifr.ifr_name, "en0", IFNAMSIZ-1);
    
    ioctl(fd, SIOCGIFADDR, &ifr);
    
    close(fd);
    
    return inet_ntoa(((struct sockaddr_in*)&ifr.ifr_addr)->sin_addr);
}
