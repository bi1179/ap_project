#define BUF_SIZE 1024

typedef struct{
	char src_ip[16];		/* host ip addr */
	char type;				/* m: send msg, f: file transfer, e: reject message*/
    char who[30];           /* Nick name */
}msg_t;

typedef struct{
    char who[30];
    char msg[2048];
}msg_b;

void* IMW();

void *server_thrd(void*args);
void client_thrd(char ip[]);

void error_handling(char *message){
    fputs(message, stderr);
    fputc('\n', stderr);
    exit(1);
}

/* arg안에 소켓이랑 추가 메세지 구조체 포함 필요. */
void send_msg(int sd, msg_b d)   // send thread main
{
    int sock=sd;
    int len;
    //while((len = write(sock, msg, strlen(msg))) == 0);
    len = send(sock, (void*)&d, sizeof(d), 0);
    if(len == -1){
        perror("message send error");
        exit(1);
    }
}

void * recv_msg(void * arg)     // read thread main
{
    msg_b b;
    int sock=*((int*)arg);
    int str_len;
    
    while(1)
    {
        str_len=recv(sock, (void*)&b, sizeof(b), 0);
        if(str_len==-1){
            close(sock);
            pthread_exit((void*)1);
        }
        pthread_mutex_lock(&mux);
        fprintf(stdout, "\nFrom %s > %s\n", b.who, b.msg);
        pthread_mutex_unlock(&mux);
    }
    close(sock);
    pthread_exit((void*)1);
}

void Mth_Send_IMW(char *ip, msg_t data){
    int sockfd;
    int clnt_len;
    int state;
    
    struct sockaddr_in serv_addr;
    
    clnt_len = sizeof(serv_addr);
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if(sockfd <0){
        perror("socket error: ");
        exit(0);
    }
    
    bzero(&serv_addr, sizeof(serv_addr));
    serv_addr.sin_family=AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr(ip);
    serv_addr.sin_port = htons(60070);
    
    sendto(sockfd, (void *)&data, sizeof(data), 0, (struct sockaddr*)&serv_addr, clnt_len);
    
    close(sockfd);
}
